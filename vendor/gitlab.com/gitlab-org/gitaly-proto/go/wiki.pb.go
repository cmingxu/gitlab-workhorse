// Code generated by protoc-gen-go. DO NOT EDIT.
// source: wiki.proto

package gitaly

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type WikiPageVersion struct {
	Commit *GitCommit `protobuf:"bytes,1,opt,name=commit" json:"commit,omitempty"`
	Format string     `protobuf:"bytes,2,opt,name=format" json:"format,omitempty"`
}

func (m *WikiPageVersion) Reset()                    { *m = WikiPageVersion{} }
func (m *WikiPageVersion) String() string            { return proto.CompactTextString(m) }
func (*WikiPageVersion) ProtoMessage()               {}
func (*WikiPageVersion) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{0} }

func (m *WikiPageVersion) GetCommit() *GitCommit {
	if m != nil {
		return m.Commit
	}
	return nil
}

func (m *WikiPageVersion) GetFormat() string {
	if m != nil {
		return m.Format
	}
	return ""
}

type WikiGetPageVersionsRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	PagePath   []byte      `protobuf:"bytes,2,opt,name=page_path,json=pagePath,proto3" json:"page_path,omitempty"`
}

func (m *WikiGetPageVersionsRequest) Reset()                    { *m = WikiGetPageVersionsRequest{} }
func (m *WikiGetPageVersionsRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiGetPageVersionsRequest) ProtoMessage()               {}
func (*WikiGetPageVersionsRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{1} }

func (m *WikiGetPageVersionsRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiGetPageVersionsRequest) GetPagePath() []byte {
	if m != nil {
		return m.PagePath
	}
	return nil
}

type WikiGetPageVersionsResponse struct {
	Versions []*WikiPageVersion `protobuf:"bytes,1,rep,name=versions" json:"versions,omitempty"`
}

func (m *WikiGetPageVersionsResponse) Reset()                    { *m = WikiGetPageVersionsResponse{} }
func (m *WikiGetPageVersionsResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiGetPageVersionsResponse) ProtoMessage()               {}
func (*WikiGetPageVersionsResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{2} }

func (m *WikiGetPageVersionsResponse) GetVersions() []*WikiPageVersion {
	if m != nil {
		return m.Versions
	}
	return nil
}

func init() {
	proto.RegisterType((*WikiPageVersion)(nil), "gitaly.WikiPageVersion")
	proto.RegisterType((*WikiGetPageVersionsRequest)(nil), "gitaly.WikiGetPageVersionsRequest")
	proto.RegisterType((*WikiGetPageVersionsResponse)(nil), "gitaly.WikiGetPageVersionsResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for WikiService service

type WikiServiceClient interface {
	WikiGetPageVersions(ctx context.Context, in *WikiGetPageVersionsRequest, opts ...grpc.CallOption) (WikiService_WikiGetPageVersionsClient, error)
}

type wikiServiceClient struct {
	cc *grpc.ClientConn
}

func NewWikiServiceClient(cc *grpc.ClientConn) WikiServiceClient {
	return &wikiServiceClient{cc}
}

func (c *wikiServiceClient) WikiGetPageVersions(ctx context.Context, in *WikiGetPageVersionsRequest, opts ...grpc.CallOption) (WikiService_WikiGetPageVersionsClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[0], c.cc, "/gitaly.WikiService/WikiGetPageVersions", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiGetPageVersionsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type WikiService_WikiGetPageVersionsClient interface {
	Recv() (*WikiGetPageVersionsResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiGetPageVersionsClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiGetPageVersionsClient) Recv() (*WikiGetPageVersionsResponse, error) {
	m := new(WikiGetPageVersionsResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Server API for WikiService service

type WikiServiceServer interface {
	WikiGetPageVersions(*WikiGetPageVersionsRequest, WikiService_WikiGetPageVersionsServer) error
}

func RegisterWikiServiceServer(s *grpc.Server, srv WikiServiceServer) {
	s.RegisterService(&_WikiService_serviceDesc, srv)
}

func _WikiService_WikiGetPageVersions_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(WikiGetPageVersionsRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(WikiServiceServer).WikiGetPageVersions(m, &wikiServiceWikiGetPageVersionsServer{stream})
}

type WikiService_WikiGetPageVersionsServer interface {
	Send(*WikiGetPageVersionsResponse) error
	grpc.ServerStream
}

type wikiServiceWikiGetPageVersionsServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiGetPageVersionsServer) Send(m *WikiGetPageVersionsResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _WikiService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "gitaly.WikiService",
	HandlerType: (*WikiServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "WikiGetPageVersions",
			Handler:       _WikiService_WikiGetPageVersions_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "wiki.proto",
}

func init() { proto.RegisterFile("wiki.proto", fileDescriptor12) }

var fileDescriptor12 = []byte{
	// 258 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x91, 0xcf, 0x4a, 0xf3, 0x40,
	0x14, 0xc5, 0xbf, 0x7c, 0x42, 0x68, 0x6f, 0x0a, 0xe2, 0x08, 0x1a, 0xd2, 0x4d, 0x18, 0x37, 0x71,
	0x13, 0x24, 0x7d, 0x04, 0x17, 0xdd, 0x96, 0x51, 0x74, 0x29, 0xd3, 0x7a, 0x4d, 0x2e, 0x35, 0x9d,
	0xe9, 0xcc, 0xb5, 0xd2, 0xb7, 0x97, 0xfc, 0x69, 0x09, 0x12, 0x5c, 0xce, 0x39, 0x67, 0xce, 0x6f,
	0x0e, 0x03, 0xf0, 0x4d, 0x5b, 0xca, 0xad, 0x33, 0x6c, 0x44, 0x58, 0x12, 0xeb, 0xcf, 0x63, 0x32,
	0xf3, 0x95, 0x76, 0xf8, 0xde, 0xa9, 0xf2, 0x19, 0x2e, 0x5f, 0x69, 0x4b, 0x2b, 0x5d, 0xe2, 0x0b,
	0x3a, 0x4f, 0x66, 0x27, 0xee, 0x21, 0xdc, 0x98, 0xba, 0x26, 0x8e, 0x83, 0x34, 0xc8, 0xa2, 0xe2,
	0x2a, 0xef, 0x6e, 0xe6, 0x4b, 0xe2, 0xc7, 0xd6, 0x50, 0x7d, 0x40, 0xdc, 0x40, 0xf8, 0x61, 0x5c,
	0xad, 0x39, 0xfe, 0x9f, 0x06, 0xd9, 0x54, 0xf5, 0x27, 0x59, 0x43, 0xd2, 0xb4, 0x2e, 0x91, 0x07,
	0xc5, 0x5e, 0xe1, 0xfe, 0x0b, 0x3d, 0x8b, 0x02, 0xc0, 0xa1, 0x35, 0x9e, 0xd8, 0xb8, 0x63, 0x0f,
	0x11, 0x27, 0x88, 0x3a, 0x3b, 0x6a, 0x90, 0x12, 0x73, 0x98, 0x5a, 0x5d, 0xe2, 0x9b, 0xd5, 0x5c,
	0xb5, 0xb0, 0x99, 0x9a, 0x34, 0xc2, 0x4a, 0x73, 0x25, 0x15, 0xcc, 0x47, 0x71, 0xde, 0x9a, 0x9d,
	0x47, 0xb1, 0x80, 0xc9, 0xa1, 0xd7, 0xe2, 0x20, 0xbd, 0xc8, 0xa2, 0xe2, 0xf6, 0x44, 0xfb, 0xb5,
	0x5d, 0x9d, 0x83, 0xc5, 0x1e, 0xa2, 0xc6, 0x7c, 0x42, 0x77, 0xa0, 0x0d, 0x8a, 0x35, 0x5c, 0x8f,
	0x20, 0x84, 0x1c, 0x16, 0x8d, 0xcf, 0x4d, 0xee, 0xfe, 0xcc, 0x74, 0x6f, 0x94, 0xff, 0x1e, 0x82,
	0x75, 0xd8, 0x7e, 0xc9, 0xe2, 0x27, 0x00, 0x00, 0xff, 0xff, 0x7d, 0x27, 0x74, 0x80, 0xb6, 0x01,
	0x00, 0x00,
}
